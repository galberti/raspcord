module gitlab.com/galberti/raspcord

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.3-0.20210529215543-f5bb723db8d9
	gitlab.com/galberti/gopus v0.0.0-20201127141821-6e32fbc67726 // indirect
)
