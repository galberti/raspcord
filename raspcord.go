
package main

import (
    "bufio"
    "bytes"
    "flag"
    "fmt"
    "net"
    "os"
    "strings"
    "syscall"
    "time"

    "encoding/binary"
    "os/exec"
    "os/signal"
    "github.com/bwmarrin/discordgo"
    "gitlab.com/galberti/gopus"
)

const (
    DEBUGLL = iota
    DEBUG
    INFO
    WARN
    FATAL
)

const OPUS_BITRATE = 48000

func err(s string, e error) {
    fmt.Printf("%s: %s\n", s, e.Error())
}

func debug(s string, lvl uint) {
    if lvl >= *debugl {
        fmt.Printf(s)
    }
}

var fromdisc_c chan *discordgo.Packet
var todisc_c chan []byte

func discord_ready(s *discordgo.Session, event *discordgo.Ready) {
    vc, e := s.ChannelVoiceJoin(*guildid, *chanid, false, false)
    if e != nil {
        err("voiceJoin()", e)
        return
    }
    debug("voice channel joined\n", INFO)
    fromdisc_c = vc.OpusRecv
    todisc_c = vc.OpusSend
}

func process_command(cmd []string) {
}

func discord_message(s *discordgo.Session, m *discordgo.MessageCreate) {
    if m.Author.ID != *boss  {
        // TODO courtesy message?
        return
    }
    pcmd := strings.Split(m.Content, " ")
    if len(pcmd) > 0 {
        process_command(pcmd)
    }
}

func discord_init() error {
    dg, e := discordgo.New("Bot " + *token)
    if e != nil {
        return e
    }
    dg.AddHandler(discord_ready)
    dg.AddHandler(discord_message)
    return dg.Open()
}

func discord_client_main() {
    e := discord_init()
    if e != nil {
        err("discord_init()", e)
        return
    }
    opusdec, _ := gopus.NewDecoder(OPUS_BITRATE, 1)
    opusenc, _ := gopus.NewEncoder(OPUS_BITRATE, 1, gopus.Voip)
    debug("wait discordgo to populate voice channels..\n", DEBUG)
    for {
        if (fromdisc_c != nil) && (todisc_c != nil) {
            break
        }
        time.Sleep(100 * time.Millisecond)
    }
    debug("voice channel populated.\n", DEBUG)
    // rtl -> discord thread
    go func(){
        buf := make([]byte, 1024)
        obuf := make([]byte, 1920) // 20ms @ 48000 samples/sec, 16 bit
        s := 0
        for {
            n, addr, e := mc.ReadFrom(buf)
            if e != nil {
                debug("ReadFrom() problem?\n", WARN)
                continue
            }
            as := addr.String()
            debug(fmt.Sprintf("voice packet from %s (len %d)\n", as, n),
                DEBUGLL)
            if (as[:8] == "192.168.") || (as[:6] == "172.17") ||
                (as[:4] == "127.") || (as[:3] == "10.") {
                copy(obuf[s:], buf[:960])
                s += n
                if s == 1920 {
                    s = 0
                    b16 := make([]int16, 960)
                    bbuf := bytes.NewBuffer(obuf[:1920])
                    binary.Read(bbuf, binary.LittleEndian, b16)
                    senc, e := opusenc.Encode(b16, len(b16), 1276)
                    if e != nil {
                        err("Opus Encode", e)
                        continue
                    }
                    todisc_c <- senc
                }
            }
        }
    }()
    // discord -> hspi loop
    lpt := time.Now()
    for {
        p := <-fromdisc_c
        if ((p.Type[1] & 0x80) == 0x80) {
            // RTCP packet, throw it away for now
            continue
        }
        pt := time.Now()
        if pt.Sub(lpt) > time.Duration(1 * time.Second) {
            // first packet in a while, buffer 200 ms
            time.Sleep(200 * time.Millisecond)
        }
        lpt = pt
        debug("got audio packet\n", DEBUGLL)
        // decode
        sdec, e := opusdec.Decode(p.Opus, OPUS_BITRATE/10, false)
        if e != nil {
            err("Opus Decode", e)
            continue
        }
        debug(fmt.Sprintf("sdec len: %d\n", len(sdec)), DEBUGLL)
        bbuf := bytes.Buffer{}
        binary.Write(&bbuf, binary.BigEndian, sdec)
        buf := bbuf.Bytes()
        if len(buf) == 1920 {
            // stream to socket
            mc.WriteTo(buf[:960], mcaddr)
            mc.WriteTo(buf[960:], mcaddr)
        }
    }
}

var mc net.PacketConn
var mcaddr net.Addr
func local_backend_main() {
    var e error
    mc, e = net.ListenPacket("udp", ":47308")
    if e != nil {
        panic(e)
    }
    mcaddr, e = net.ResolveUDPAddr("udp", "239.47.78.1:47781")
    if e != nil {
        panic(e)
    }
}

const HSPI = "/usr/src/saoud-c/hspi"
func hspi_manager() {
    for {
        cmd := exec.Command(HSPI, "-d", "-q", *sq, "-t", *txf, "-r", *rxf,
            "-k", *ppm)
        stdio, e := cmd.StdoutPipe()
        if e != nil {
            panic(e)
        }
        stderr, e := cmd.StderrPipe()
        if e != nil {
            panic(e)
        }
        go func(){
            out := bufio.NewReader(stdio)
            for {
                b, e := out.ReadBytes('\n')
                if e != nil {
                    err("hspi out pipe", e)
                    break
                }
                debug(fmt.Sprintf("hspi out: %s", string(b)), DEBUGLL)
            }
        }()
        go func(){
            out := bufio.NewReader(stderr)
            for {
                b, e := out.ReadBytes('\n')
                if e != nil {
                    err("hspi err pipe", e)
                    break
                }
                debug(fmt.Sprintf("hspi err: %s", string(b)), DEBUGLL)
            }
        }()
        e = cmd.Run()
        if e != nil {
            err("hspi_manager", e)
            break
        }
        debug("hspi exited, respawning..\n", WARN)
    }
}

var token *string
var guildid *string
var boss *string
var chanid *string
var debugl *uint
var txf *string
var rxf *string
var sq *string
var ppm *string
func main() {
    // mandatory
    token = flag.String("k", "", "discord token")
    guildid = flag.String("s", "", "server id (guild)")
    chanid = flag.String("c", "", "channel id")
    boss = flag.String("b", "", "boss discord ID")
    debugl = flag.Uint("d", WARN, "debug")

    // hspi control - optionals
    txf = flag.String("t", "432800", "PI tx freq (432800)")
    rxf = flag.String("r", "433800", "RTL rx freq (433800)")
    sq = flag.String("q", "0.0005", "RTL squelch (0.0005)")
    ppm = flag.String("p","-45", "RTL ppm correction (-45)")

    flag.Parse()

    if *token == "" || *guildid == "" || *boss == "" {
        debug("Missing parameter.\n", FATAL)
        flag.PrintDefaults()
        return
    }

    local_backend_main()
    go hspi_manager()
    go discord_client_main()

    sc := make(chan os.Signal, 1)
    signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
    <-sc
}

